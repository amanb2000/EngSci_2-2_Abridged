# EngSci_2.2_Abridged
Engineering Science foundation year 2 semester 2 notes. Classes include PHY294, ECE253, and ECE286.

## Goals

The point of these notes is to be a review resource. Since they are my notes, they are not comprehensive in the same way that a textbook might be - I spend more time on the things I find difficult and less time on the things that I grasped more easily. Either way, they should be decent review tools to supplement other sources! 

## How to Access Notes

Navigate to the `pdf` folder to find the most recently updated exports of my notes. If you want to play with and modify the LaTeX files yourself, go to the `tex` folder and have fun.

