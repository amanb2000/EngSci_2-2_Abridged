\contentsline {chapter}{\numberline {1}Electrostatics}{2}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Coulomb's Law and Electric Field}{2}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Electric Field Intensity $\mathaccentV {vec}17E{E}$}{2}{section.1.2}% 
\contentsline {paragraph}{Electric Field at $R_2$ due to $Q_1$ at $R_1$: }{2}{section*.2}% 
\contentsline {paragraph}{Properties of $\mathaccentV {vec}17E{E}$}{2}{section*.3}% 
\contentsline {section}{\numberline {1.3}$\mathaccentV {vec}17E{E}$ from Continuous Charge Distribution}{3}{section.1.3}% 
\contentsline {paragraph}{Steps for Finding $\mathaccentV {vec}17E{E}_{tot}$:}{3}{section*.4}% 
\contentsline {section}{\numberline {1.4}Gauss's Law}{3}{section.1.4}% 
\contentsline {subsection}{\numberline {1.4.1}Fundamental Postulates of Electrostatics}{3}{subsection.1.4.1}% 
\contentsline {paragraph}{What is $\mathaccentV {vec}17E{D}$? }{3}{section*.5}% 
\contentsline {subsection}{\numberline {1.4.2}Why is Gauss's law true?}{4}{subsection.1.4.2}% 
\contentsline {subsection}{\numberline {1.4.3}Problem Solving with Gauss's Law}{4}{subsection.1.4.3}% 
\contentsline {paragraph}{Gaussian Surface Properties: }{4}{section*.6}% 
\contentsline {paragraph}{Steps to getting $\mathaccentV {vec}17E{E}$ with Gauss's Law:}{4}{section*.7}% 
\contentsline {section}{\numberline {1.5}Electric Scalar Potential}{5}{section.1.5}% 
\contentsline {paragraph}{Absolute $V$: }{5}{section*.8}% 
\contentsline {paragraph}{Equipotential surfaces: }{5}{section*.9}% 
\contentsline {paragraph}{Steps for Problem Solving: }{5}{section*.10}% 
\contentsline {paragraph}{Electric Potential of a Point Charge: }{6}{section*.11}% 
\contentsline {section}{\numberline {1.6}Dielectrics}{6}{section.1.6}% 
\contentsline {paragraph}{Surface-Bound Charge: }{6}{section*.12}% 
\contentsline {subsection}{\numberline {1.6.1}Dielectric Strength}{7}{subsection.1.6.1}% 
\contentsline {subsection}{\numberline {1.6.2}Dielectric Boundary Conditions}{7}{subsection.1.6.2}% 
\contentsline {paragraph}{Resultant properties of boundary conditions: }{7}{section*.13}% 
\contentsline {subsection}{\numberline {1.6.3}Capacitance}{7}{subsection.1.6.3}% 
\contentsline {paragraph}{Capacitors: }{8}{section*.14}% 
\contentsline {section}{\numberline {1.7}Electrostatic Energy}{8}{section.1.7}% 
\contentsline {paragraph}{Energy density: }{8}{section*.15}% 
\contentsline {paragraph}{Two Paths to Energy $W_e$}{8}{section*.16}% 
\contentsline {section}{\numberline {1.8}LaPlace and Poisson's Equations}{9}{section.1.8}% 
\contentsline {paragraph}{Some vocabulary: }{9}{section*.17}% 
\contentsline {paragraph}{Notes on solving the BVP: }{9}{section*.18}% 
\contentsline {paragraph}{Procedure for BVP's: }{10}{section*.19}% 
\contentsline {paragraph}{Electric Shielding}{10}{section*.20}% 
\contentsline {section}{\numberline {1.9}Resistance and Joule's Law}{10}{section.1.9}% 
\contentsline {subsection}{\numberline {1.9.1}Volume Current Density}{10}{subsection.1.9.1}% 
\contentsline {paragraph}{Resistance: }{11}{section*.21}% 
\contentsline {subsection}{\numberline {1.9.2}Power}{11}{subsection.1.9.2}% 
